#include <iostream>

int
main() 
{
    int R, d, l, S;	

    std::cout << "Enter radius: ";
    std::cin  >> R ;

    d = 2 * R;
    l = 2 * R;
    S = R * R;

    std::cout << "Diameter = " << d;
    std::cout << "\nThe circumference = " << l * 3.14159;
    std::cout << "\nSquare = " << S * 3.14159 << std::endl;

    return 0;
}	


