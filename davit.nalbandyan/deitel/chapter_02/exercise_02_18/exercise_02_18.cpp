#include <iostream>

int
main()
{
    int a,b;
    std::cout << "Enter two numbers" << std::endl;
    std::cin >> a >> b;
    if(a > b){
        std::cout << "More " << a << std::endl;
    }
    if(b > a){
        std::cout << "More " << b << std::endl;
    }
    if(a == b){
        std::cout << "These numbers are equal" << std::endl;
    }

    return 0;
}
