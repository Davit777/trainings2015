#include <iostream> /// Definitions Invoice element functions
#include "Invoice.hpp" /// Implementation of design’s Invoice

Invoice::Invoice(std::string article, std::string description, int quantity, int price) /// the constructor initializes parameter
{  
    /// initializes a set-function call
    setArticle(article);
    setDescription(description);
    setQuantity(quantity);
    setPrice(price);
}

void 
Invoice::setArticle(std::string article) /// funcion set the name of Article
{
    article_ = article; /// the member-variable appropriates name of parameter
}

void
Invoice::setDescription(std::string description) /// function set the name of Article
{
    description_ = description; /// the member-variable appropriates name of parameter
}

void
Invoice::setQuantity(int quantity) /// function set the value of Quantity
{
    if(quantity >= 0) { /// checks the imported sum
        quantity_ = quantity; /// the member-variable appropriates value of parameter
    }
    if(quantity < 0) { /// if the sum is smaller than zero
        quantity_ = 0; /// appropriate zero to member-variable
        std::cout << "The entered quantity " << quantity << " is invalid. It is reset to 0." << std::endl; /// output the message when function gives error
    }
}

void
Invoice::setPrice(int price) /// function set the value of Price
{
    if(price >= 0) { /// checks the imported value
        price_ = price; /// the member-variable appropriats value of parameter
    }
    if(price < 0) { /// if the value is smaller than zero
        price_ = 0; /// appropriates zero to member-variable
        std::cout << "The enterd price " << price << " is invalid. Now it is 0." << std::endl; /// output the message when function gives error
    }
}
   
std::string
Invoice::getArticle() /// function get name of Article
{
    return article_;
}

std::string
Invoice::getDescription() /// function get name of Description
{
    return description_;
}

int
Invoice::getQuantity() /// function get value of Quantity
{
    return quantity_;
}

int
Invoice::getPrice() /// function get value of Price
{
    return price_;
}

int
Invoice::getInvoiceAmount() /// function calculate the total amount and get the value 
{
   return quantity_ * price_;
}
