#include <iostream>

int
main()
{
    int fiveDigitNumber;
    std::cout << "Enter five-digit number: ";
    std::cin >> fiveDigitNumber;
    if(fiveDigitNumber > 99999) {
        std::cerr << "Error1. This is not a five-digit number." << std::endl;
        return 1;
    } else if (fiveDigitNumber < 10000) {
        std::cerr << "Error1. This is not a five-digit number." << std::endl;
        return 1;
    }
    
    if(fiveDigitNumber / 10000 == fiveDigitNumber % 10) {
        if(fiveDigitNumber % 100 / 10 == fiveDigitNumber / 1000 % 10) {
            std::cout << "This number is palindrom." << std::endl;
        } else {
            std::cout << "This number isn`t palindrom." << std::endl;
        }
    } else {
        std::cout << "This number isn`t palindrom." << std::endl;
    }
    
    return 0;
} 
