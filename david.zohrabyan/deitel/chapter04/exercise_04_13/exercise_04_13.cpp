/* The program calculates and displays number of miles on  
gallon for each gas station, and also a general meaning of miles on gallon on all  
to the gas stations entered by this moment. */

#include <iostream> /// standard input output                        
#include <iomanip> /// the parametrized stream manipulators
 
/// function main begins program execution                                         
int                                   
main()                                   
{                                  
    int miles;                                                                                      
    std::cout << "Enter miles driven (less than 0 to quit): ";           
    std::cin >> miles;                                             
    
    double totalMiles = 0.0, totalGallons = 0.0;  
    
    while(miles > 0)
    {
        int gallons;                                    
        std::cout << "Enter a gasoline consumption: ";                               
        std::cin >> gallons;
        if(gallons <= 0) {
            std::cerr << "Error 1: Entered gasoline consumption should be positive!" << std::endl;
            return 1;
        }                                                           
           
        double oneMileGallon = static_cast<double>(miles) / gallons; 
        std::cout << "Miles/gallon for this gas station: " << std::setprecision(6) << std::fixed << oneMileGallon << std::endl;    
        
        totalMiles += miles;
        totalGallons += gallons;
       
        double totalMileGallon = totalMiles / totalGallons;
        std::cout << "Total value of miles/gallon: " << std::setprecision(6) << std::fixed << totalMileGallon << std::endl;      
          
        std::cout << "Enter miles driven (less than 0 to quit): ";     
        std::cin >> miles;                   
    }
   
    return 0;                                                                      
}
