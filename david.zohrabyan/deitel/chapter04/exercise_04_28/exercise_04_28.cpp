#include <iostream>

int
main()
{
    int row = 0;
    
    while(row < 8)
    {
        if(0 != row % 2) {
            std::cout << ' ';
        }
        
        int column = 0;
        while(column < 8)
        {
            std::cout << "* ";
            ++column;
        }
        ++row;
        std::cout << std::endl;
    }
    
    return 0;
}   
