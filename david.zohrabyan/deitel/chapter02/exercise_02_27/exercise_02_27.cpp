/// This program defines integer equivalents of the following symbols "A,B,C,a,b,c,0,1,2,$,*,+,/,space
#include <iostream> /// standard input output

/// function main begins program execution
int
main()
{
    std::cout << static_cast<int>('A') << std::endl; /// dipslay intger equivalent of "A";end line
    std::cout << static_cast<int>('B') << std::endl; /// dipslay intger equivalent of "B";end line
    std::cout << static_cast<int>('C') << std::endl; /// dipslay intger equivalent of "C";end line
    std::cout << static_cast<int>('a') << std::endl; /// dipslay intger equivalent of "a";end line
    std::cout << static_cast<int>('b') << std::endl; /// dipslay intger equivalent of "b";end line
    std::cout << static_cast<int>('c') << std::endl; /// dipslay intger equivalent of "c";end line
    std::cout << static_cast<int>('0') << std::endl; /// dipslay intger equivalent of "0";end line
    std::cout << static_cast<int>('1') << std::endl; /// dipslay intger equivalent of "1";end line
    std::cout << static_cast<int>('2') << std::endl; /// dipslay intger equivalent of "2";end line
    std::cout << static_cast<int>('$') << std::endl; /// dipslay intger equivalent of "$";end line
    std::cout << static_cast<int>('*') << std::endl; /// dipslay intger equivalent of "*";end line
    std::cout << static_cast<int>('+') << std::endl; /// dipslay intger equivalent of "+";end line
    std::cout << static_cast<int>('/') << std::endl; /// dipslay intger equivalent of "/";end line
    std::cout << static_cast<int>(' ') << std::endl; /// dipslay intger equivalent of "space";end line

    return 0; /// program ended successfully
} /// end function main
