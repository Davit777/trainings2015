/// This program reads out an integer and it defines even or odd
#include <iostream> /// standard input output
 
/// function main begins program execution
int 
main()
{
    int number1; /// announcement of variable
 
    std::cout << "number1:" << std::endl; /// prompt user for data
    std::cin >> number1; /// read number from user into "number1"
  
    if (number1 % 2 == 0) { /// solves on the module, does the number equal to zero
       std::cout << "The " << number1 << " is even number " << std::endl; /// display number1 and text;end line
    }
    if (number1 % 2 != 0) { /// solves on the module,does the number unequal to zero
       std::cout << "The " << number1 << " is odd number " << std::endl; /// display number1 and text;end line
    }
    return 0; /// program ended successfully
} /// end function main
