#include <iostream>

int
main()
{
    int number1, number2, number3, max, min;
   
    std::cout << "Enter three various integers: ";
    std::cin >> number1 >> number2 >> number3;

    std::cout << "The sum is " << number1 + number2 + number3 << std::endl;

    std::cout << "The average value is " << (number1 + number2 + number3) / 3 << std::endl;

    std::cout << "The mul is " << number1 * number2 * number3 << std::endl;

    max = number1;
    if (number2 > max) {
        max = number2;
    }
    if (number3 > max) {
        max = number3;
    }
    std::cout << "The max is " << max << std::endl;

    min = number1;
    if (number2 < min) {
        min = number2;
    } 
    if (number3 < min) {
        min = number3;
    }
    std::cout << "The min is " << min << std::endl;
                           
    return 0;
}    
