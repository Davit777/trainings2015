The default constructor is a special member function without parameters or
with default parameters (all parameters must have dafailt value) and 
without return value.
If a class does not explicitly include constructors, the compiler provides a 
default constructor with no parameters, or we type it explicitly.
The implicitly definded default constructor implicitly calls each data member's
default constructor, for fundamental type data members it gives them garbage
initial values.
