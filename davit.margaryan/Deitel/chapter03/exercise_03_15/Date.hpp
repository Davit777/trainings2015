#ifndef __DATE_HPP__
#define __DATE_HPP__

/// Date.hpp
/// Date class definition the public interface of the class

/// Date class defintion
class Date
{
public:
    /// constructor intilaize data members
    Date(int month, int day, int year);
    void setMonth(int mounth); /// sets the mounth
    int getMonth(); /// gets the mount
    void setDay(int day); /// sets the day
    int getDay(); /// gets the day
    void setYear(int year); /// sets the year
    int getYear(); /// gets the year
    void displayDate(); /// displays date
private:
    int month_;
    int day_;
    int year_;
}; /// end class Date

#endif /// __DATE_HPP__
