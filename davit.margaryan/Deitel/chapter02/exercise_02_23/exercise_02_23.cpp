/// The program reads in five integers and determines and prints 
/// tthe largest and the smallest integer in the group.

#include <iostream> /// allows program to use input and output

/// function main begins program execution
int
main()
{
    /// initialization of data
    int number1 = 0, number2 = 0, number3 = 0,
        number4 = 0, number5 = 0;

    /// input data
    std::cout << "Enter five integers: ";
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;

    /// initialization of min and max
    int min = number1, max = number1;
    
    /// smallest search part
    if(min > number2) {
        min = number2;
    } 
    if(min > number3) {
        min = number3;
    }
    if(min > number4) {
        min = number4;
    }
    if(min > number5) {
        min = number5;
    }
    /// largest search part
    if(max < number2) {
        max = number2;
    }
    if(max < number3) {
        max = number3;
    }
    if(max < number4) {
        max = number4;
    }
    if(max < number5) {
        max = number5;
    }
    if(min == max) { /// if max and min are equal
        std::cout << "The largest and smallest are equal: " << max << std::endl;      
        return 0; /// program ends successfully
    } /// end if

    /// print data
    std::cout << "Largest: " << max << std::endl;
    std::cout << "Smallest: " << min << std::endl;

    return 0; /// program ends successfully.
} /// end function main
