/// The program reads an integer and determines
/// and prints whether it's odd or even.

#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    /// initialization of data
    int number = 0;

    /// input data
    std::cout << "Enter a number: ";
    std::cin >> number;

    if(number % 2 == 0) { /// if number is even
        std::cout << "Even number!" << std::endl;
    } /// endif
    if(number % 2 == 1) { /// if number is odd
        std::cout << "Odd number!" << std::endl;
    } /// endif
 
    return 0; /// program ends successfully
} /// end function main
